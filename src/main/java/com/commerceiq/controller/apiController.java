package com.commerceiq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.OutputStream;
import java.util.*;
import javax.servlet.http.HttpServletResponse;
import com.commerceiq.pojo.CsvOperationPojo;
import com.commerceiq.pojo.WeeklyOperationsPojo;
import com.commerceiq.pojo.YearlyCommitPojo;
import com.commerceiq.service.CommitService;


@RestController
public class apiController {

	@Autowired
	CommitService service;
	
	@RequestMapping("/commit/yearly")
	public ResponseEntity<List<YearlyCommitPojo>>  yearlyCommitHistory()
	{
		try {
			List<YearlyCommitPojo> yearly = service.yearly();
			if(yearly != null)
				return new ResponseEntity<List<YearlyCommitPojo>>(yearly , HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping("/commit/operations")
	public ResponseEntity<List<WeeklyOperationsPojo>> weeklyCommitHistory()
	{
		try {
			List<WeeklyOperationsPojo> weekly = service.weekly();
			if(weekly != null )
				return new ResponseEntity<List<WeeklyOperationsPojo>>(weekly, HttpStatus.OK);
		}catch(Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping("commit/csv")
	public ResponseEntity<List<CsvOperationPojo>> getCombinedDataForYearlyAndWeeklyData()
	{
		try {
			List<CsvOperationPojo> csvList = service.mergeOfYearlyAndWeeklyData();
			if(csvList != null)
				return new ResponseEntity<List<CsvOperationPojo>>(csvList , HttpStatus.OK);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}


	@RequestMapping("commit/csv/download")
	public void downloadCsv(HttpServletResponse response)
	{
		List<String> fileReadList = service.init();
		response.setContentType("text/csv");
	    response.setHeader("Content-Disposition", "attachment; filename=\"dataset.csv\"");
		try {
			OutputStream outputStream = response.getOutputStream();
			String header = "week" + "," + "total" + "," + "addition" + "," + "deletion";
			outputStream.write(header.toString().getBytes());
			outputStream.write("\n".toString().getBytes());
			for(int i  = 0 ; i < fileReadList.size(); i++)
			{
			outputStream.write(fileReadList.get(i).getBytes());
			outputStream.write("\n".toString().getBytes());
			}
			outputStream.flush();
			outputStream.close();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
}


