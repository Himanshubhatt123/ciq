package com.commerceiq.pojo;

public class WeeklyOperationsPojo {
	
	private Long week;
	private Long addition;
	private Long deletion;
	
	
	
	
	public WeeklyOperationsPojo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public WeeklyOperationsPojo(Long week, Long addition, Long deletion) {
		super();
		this.week = week;
		this.addition = addition;
		this.deletion = deletion;
	}

	public Long getWeek() {
		return week;
	}

	public void setWeek(Long week) {
		this.week = week;
	}

	public Long getAddition() {
		return addition;
	}

	public void setAddition(Long addition) {
		this.addition = addition;
	}

	public Long getDeletion() {
		return deletion;
	}

	public void setDeletion(Long deletion) {
		this.deletion = deletion;
	}

	
	
	
	
	
}
