package com.commerceiq.pojo;

public class CsvOperationPojo {

	private Long week;
	private Long total;
	private Long addition;
	private Long deletion;
	
	
	
	
	public CsvOperationPojo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CsvOperationPojo(Long week, Long total, Long addition, Long deletion) {
		super();
		this.week = week;
		this.total = total;
		this.addition = addition;
		this.deletion = deletion;
	}
	
	
	public Long getWeek() {
		return week;
	}
	public void setWeek(Long week) {
		this.week = week;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public Long getAddition() {
		return addition;
	}
	public void setAddition(Long addition) {
		this.addition = addition;
	}
	public Long getDeletion() {
		return deletion;
	}
	public void setDeletion(Long deletion) {
		this.deletion = deletion;
	}
	
	
	
}
