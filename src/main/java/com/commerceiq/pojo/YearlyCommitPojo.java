package com.commerceiq.pojo;

import java.util.*;


public class YearlyCommitPojo {
	
	private Long total;
	private Long week;
	private List<Integer> days = new ArrayList();
	
	public YearlyCommitPojo() {
		
	}
	
	public YearlyCommitPojo(Long total, Long week, List<Integer> days) {
		super();
		this.total = total;
		this.week = week;
		this.days = days;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getWeek() {
		return week;
	}

	public void setWeek(Long week) {
		this.week = week;
	}

	public List<Integer> getDays() {
		return days;
	}

	public void setDays(List<Integer> days) {
		this.days = days;
	}
	
	
	
	
	
}
