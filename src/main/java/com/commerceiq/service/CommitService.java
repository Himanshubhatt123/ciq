package com.commerceiq.service;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.commerceiq.pojo.*;
import com.opencsv.CSVWriter;

@Service
public class CommitService {

	final String Commit_activity ="https://api.github.com/repos/GoogleCloudPlatform/microservices-demo/stats/commit_activity";
	final String Code_frequency = "https://api.github.com/repos/GoogleCloudPlatform/microservices-demo/stats/code_frequency";
	public static final String filePath = "data.csv";
	
	
	public List<YearlyCommitPojo> yearly() {

		RestTemplate restTemplate = new RestTemplate();
		YearlyCommitPojo[] mapData = restTemplate.getForObject(Commit_activity,YearlyCommitPojo[].class);

		return Arrays.asList(mapData);
	}


	public List<WeeklyOperationsPojo> weekly() {

		RestTemplate restTemplate = new RestTemplate();
		Object[] mapData = restTemplate.getForObject(Code_frequency,Object[].class);

		List<WeeklyOperationsPojo> freqlist  = new ArrayList<>();

		for(int i  = 0 ; i < mapData.length; i++) 
		{
			List<Integer> weeklyList = (List<Integer>) mapData[i];

			WeeklyOperationsPojo freq = new WeeklyOperationsPojo();

			freq.setWeek(Long.valueOf(weeklyList.get(0)));
			freq.setAddition(Long.valueOf(weeklyList.get(1)));
			freq.setDeletion(Long.valueOf(weeklyList.get(2)));

			freqlist.add(freq);

			weeklyList.clear();
		}

		return freqlist;
	}

	public List<CsvOperationPojo> mergeOfYearlyAndWeeklyData()
	{
		List<YearlyCommitPojo> yearlyList = yearly();
		List<WeeklyOperationsPojo> weeklyList =  weekly();


		List<CsvOperationPojo> freqlist  = new ArrayList<>();

		for(int i = 0 ; i < yearlyList.size(); i++)
		{
			for(int j =  0 ; j < weeklyList.size(); j++ )
			{
				if(yearlyList.get(i).getWeek().equals(weeklyList.get(j).getWeek()))
				{
					CsvOperationPojo csvOperate  = new CsvOperationPojo();

					csvOperate.setWeek(yearlyList.get(i).getWeek());
					csvOperate.setTotal(yearlyList.get(i).getTotal());
					csvOperate.setAddition(weeklyList.get(j).getAddition());
					csvOperate.setDeletion(weeklyList.get(j).getDeletion());

					freqlist.add(csvOperate);
				}
			}
		}
		return freqlist;
	}

	public static Path namedColumnCsvPath() throws URISyntaxException {
		URI uri = ClassLoader.getSystemResource(filePath).toURI();
		return Paths.get(uri);
	}


	public void writeFile(Path path) {
		System.out.println("Inside write file ");
		File file = new File(path.toString()); 
		try { 
			FileWriter outputfile = new FileWriter(file); 

			CSVWriter writer = new CSVWriter(outputfile); 

			List<CsvOperationPojo> list = mergeOfYearlyAndWeeklyData();
			for(int i = 0 ; i < list.size(); i++ )
			{
				String[] header = { list.get(i).getWeek().toString(), list.get(i).getTotal().toString(), 
						list.get(i).getAddition().toString(), list.get(i).getDeletion().toString() }; 
				writer.writeNext(header); 	
				System.out.println(header);
			}
			writer.close();
		} 
		catch (Exception e) { 
			e.printStackTrace(); 
		}
	}



	public List<String> readFile(Path path)
	{
		List<String> responseList = new ArrayList<>();
		try {
			
			FileReader fr = new FileReader(path.toString());
			BufferedReader br = new BufferedReader(fr);
			String strLine;
			while ((strLine = br.readLine()) != null) {
				System.out.println("strLine "+strLine);
				responseList.add(strLine);
			}
			fr.close();
			br.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return responseList;
	}


	public List<String> init() {
		Path path;
		List<String> responseList  = null; 
		try {
			path = namedColumnCsvPath();
			writeFile(path);
			responseList = readFile(path);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return responseList;
	}
}

