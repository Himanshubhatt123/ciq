# commerceIQ

REQUIREMENT : 
        Language :Java 
        Framework: spring
        Ide: springtoolsuit, eclipse, itelliJ


INSTALLATION:

1. git clone https://gitlab.com/Himanshubhatt123/commerceiq.git
2. import project in above given ide.
3. run commerceiqApplication.java class


API 
1. endpoint : http://localhost:8080/commit/yearly
info: gives yearly data.

2. endpoint : http://localhost:8080/commit/operations 
       info : gives no of addition and deletion per week

3. endpoint : http://localhost:8080/commit/csv/download
       info : downloads data in .csv format 


owner used  : GoogleCloudPlatform
repo used   : microservices-demo